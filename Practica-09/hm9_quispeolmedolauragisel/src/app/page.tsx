import Button from "@/components/Button";
import Plans from "@/components/Plans";
import Pokemon from "@/components/Pokemon";
import Video from "@/components/Video";
import Image from "next/image";

export default function Home() {
  return (
    <main>
      <div className="content-button">
        <Button title="HOLA"></Button>
        <Button title="ADIOS" transparent></Button>
      </div>

      <div className="pokemon-go">
        <Pokemon title="Evee" image="/images/Eevee.webp" type="Tipo: Normal" />
        <Pokemon title="Articuno" image="/images/Articuno.png" type="Tipo: Hielo y Volador" />
        <Pokemon title="Mewtwo" image="/images/Mewtwo.png" type="Tipo: Psiquico" />
      </div>

      <div className="videos">
        <Video title="Older" author="Sasha Sloan" video="/videos/Older.mp4" />
        <Video title="Older" author="Sasha Sloan" video="/videos/Older.mp4" />
        <Video title="Older" author="Sasha Sloan" video="/videos/Older.mp4" />
      </div>

      <div className="service">
        <Plans image="/images/free.svg" text="Free Plan" list="Unlimited Bandwitch" list1="Encrypted Connection" list2="No Traffic Logs" list3="Works on All Devices" price="Free / mo"></Plans>
        <Plans image="/images/Standard.svg" text="Standard Plan" list="Unlimited Bandwitch" list1="Encrypted Connection" list2="No Traffic Logs" list3="Works on All Devices" price="$9 / mo"></Plans>
        <Plans image="/images/premium.svg" text="Premium Plan" list="Unlimited Bandwitch" list1="Encrypted Connection" list2="No Traffic Logs" list3="Works on All Devices" price="$12 / mo"></Plans>
      </div>
    </main>
  );
}