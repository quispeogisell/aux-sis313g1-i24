export default function Plans (props:{image:string,text:string,list:string,list1:string,list2:string,list3:string,price:string}) {
    return(
        <div className="item-services">
            <div>
                <img src={props.image} alt=""></img>
            </div>
            <h3>{props.text}</h3>
            <ul>
                <li><img src="images/icons.svg" alt="" />{props.list}</li>
                <li><img src="images/icons.svg" alt="" />{props.list1}</li>
                <li><img src="images/icons.svg" alt="" />{props.list2}</li>
                <li><img src="images/icons.svg" alt="" />{props.list3}</li>
            </ul>
        </div>
    )
}