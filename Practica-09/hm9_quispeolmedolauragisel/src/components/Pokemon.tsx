export default function Pokemon(props:{title:string, image:string, type:string}){
    return (
        <div className="item-pokemon">
            <h1>{props.title}</h1>
            <div>
                <img src={props.image} alt=""></img>
            </div>
            <p>{props.type}</p>
        </div>
    )
}