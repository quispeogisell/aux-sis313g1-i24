export default function Video(props:{title:string, author:string, video:string}){
    return (
        <div className="item-video">
            <h3>{props.title}</h3>
            <p>{props.author}</p>
            <video controls>
                <source src="/videos/Older.mp4" type="" />
            </video>
        
        </div>
    )
}