
# PRACTICA N°7

Docente: **Ing. Jose David Manami Figeroa** 

Auxiliar: **Univ. Sergio Moises Apaza Caballero**

Estudiante:  **Univ. Laura Gisel Quispe Olmedo** 

***Diseño y programacion grafica(SIS-313)***

## PREGUNTAS

### 1.- Realize un video explicando lo siguiente:

**a) La creacion de una carpeta en el disco local C: , mediante comandos desde la terminal.**

**b) La forma de cambiar de directorios utilizando comandos.**
 
    El enlace es el siguiente :

https://drive.google.com/file/d/1YDAQuLlqhFIzrTJdDYHNk-qMI3puHsBU/view?usp=sharing

### 2.- Adjunte capturas de pantalla del mismo proceso, tres veces mas.

        Para la creacion de la carpeta utilizamos el comando --> md

![crear](/Practica-07/Imagenes/crear.jpg)

        Para cambiar de directores utilizamos:
                cd (Para ubicarnos en un dereccion).
                cd .. (para retroceder entre los directores).

![director](/Practica-07/Imagenes/directorio.jpg)

### 3.- Que comandos utilizo en las anteriores preguntas.

    Se utilizo cuatro comandos los cuales son:
        - md
        - dir
        - cd
        - cd ..

### 4.- Explique 5 comandos para ser uilizados en la terminal y su forma de usarse en windows (Terminal o Powershell) y en Linux (bash).

**Para Windows** 

    COPY  -->  Copia uno o más archivos en otra ubicación.
    DIR   -->  Muestra una lista de archivos y subdirectorios en un directorio.
    HELP  -->  Proporciona información de Ayuda para los comandos de Windows.
    RENAME-->  Cambia el nombre de uno o más archivos.
    MOVE  -->  Mueve uno o más archivos de un directorio a otro en la misma unidad.

**Para linux**

    ls    --> Te permite listar el contenido del directorio que quieras (el directorio actual por defecto), 
                incluyendo archivos y otros directorios anidados.
    rm    --> Elimina arhivos y directores.
    touch --> Permite actualizar los tiempos de acceso y modificación de los archivos especificados.
    cat   --> Permite crear, visualizar y concatenar archivos directamente desde el terminal. 
    less  --> Permite inspeccionar archivos hacia atrás y hacia adelante.
    

