# PRACTICA N°15

Docente: **Ing. Jose David Manami Figeroa**

Auxiliar: **Univ. Sergio Moises Apaza Caballero**

Estudiante:  **Univ. Laura Gisel Quispe Olmedo** 

***Diseño y programacion grafica(SIS-313)***

## 2. Investigar los mensajes de GET, POST y PUT ¿Qué son? ¿Para qué sirven?

### GET

**¿Qué es el método GET?**

    El método GET es uno de los métodos estándar del protocolo     HTTP. Se utiliza para solicitar recursos 
    específicos del servidor mediante una URL. Esta solicitud puede incluir parámetros que se adjuntan a 
    la URL como parte de la cadena de consulta (query string).

    -Características clave:

    1.- Petición a un recurso específico: GET se utiliza para obtener datos de un recurso identificado por una URL.

    2.- Envío de datos: Aunque GET no está diseñado para enviar grandes cantidades de datos o datos sensibles, sí 
    permite enviar parámetros a través de la URL para filtrar o especificar la información solicitada.

    3.- Retorno de datos: GET retorna tanto la cabecera de la respuesta HTTP (que incluye información como el tipo de
    contenido, codificación, etc.) como el contenido solicitado en sí mismo (que puede ser en formato HTML, JSON, XML, 
    imágenes, JavaScript, u otros).

**¿Para que sirve?:**

    -Consulta de información: Semánticamente, GET se utiliza para recuperar datos del servidor, similar a cómo una 
    sentencia SQL SELECT obtiene datos de una base de datos.

    -Filtrado de datos: Los parámetros en la URL permiten filtrar o especificar qué datos deben ser recuperados 
    del servidor.

    -Semántica vs. Violación de semántica: Idealmente, GET se utiliza para operaciones de lectura (consultas), 
    donde no se espera que la solicitud modifique el estado del servidor. Sin embargo, en la práctica, se puede
    usar para otros fines (como inserciones, actualizaciones o eliminaciones), aunque esto no sigue las prácticas 
    recomendadas de diseño de API RESTful y puede resultar en problemas de seguridad y mantenibilidad.

### POST

**¿Qué es el método POST?**

    El método POST es otro de los métodos estándar del protocolo HTTP. A diferencia de GET, POST envía datos al 
    servidor a través del cuerpo (body) de la solicitud HTTP en lugar de a través de la URL. El tipo de datos 
    que se envía se especifica en la cabecera Content-Type de la solicitud HTTP.

    -Características clave:

    1.- Envío de datos: POST permite enviar datos al servidor mediante el cuerpo de la solicitud. Esto lo hace 
    adecuado para enviar grandes cantidades de datos o datos sensibles que no deben aparecer en la URL.

    2.- Tipo de cuerpo de solicitud: El formato de los datos enviados (por ejemplo, formulario HTML, JSON, XML) 
    se especifica en la cabecera Content-Type de la solicitud HTTP.

    3.- No idempotente: A diferencia de GET, POST no es idempotente, lo que significa que cada vez que se realiza 
    una solicitud POST, puede tener un efecto diferente en el estado del servidor. Por ejemplo, enviar el mismo 
    formulario dos veces puede resultar en la creación de dos entradas distintas en una base de datos.

**¿Para que sirve?:**

    Registro de información: Semánticamente, POST se utiliza para enviar datos nuevos al servidor, similar a cómo una 
    sentencia SQL INSERT registra nuevos datos en una base de datos.

    Otros usos: Aunque su principal uso es para enviar datos, POST también se puede utilizar para otras acciones como 
    actualizar registros existentes, eliminar registros, cargar archivos al servidor, entre otros. Sin embargo, utilizar 
    POST para acciones que no están relacionadas con la creación de nuevos datos puede no seguir las mejores prácticas 
    de diseño RESTful.

### PUT

**¿Qué es el método PUT?**

    PUT es un método HTTP que se utiliza para enviar datos al servidor para actualizar un recurso existente en una 
    ubicación específica. A diferencia de POST, PUT es idempotente, lo que significa que realizar la misma solicitud 
    PUT varias veces no tendrá efectos diferentes en el servidor más allá de la primera vez. Esto es porque PUT se 
    utiliza típicamente para actualizar datos en lugar de crear nuevos datos cada vez que se llama.

    -Características clave:

    1.- Actualización de datos: PUT se utiliza para enviar datos al servidor con el propósito principal de actualizar 
    un recurso existente en una ubicación determinada.

    2.- Idempotente: La idempotencia significa que repetir la misma solicitud PUT no cambia el estado del servidor más 
    allá de la primera ejecución. Esto es útil para operaciones donde garantizar que el estado del servidor no se 
    altere de manera inesperada es importante.

    3.- Tipo de cuerpo de solicitud: Al igual que POST, PUT puede enviar datos a través del cuerpo de la solicitud HTTP. 
    El formato de estos datos (como formulario HTML, JSON, XML) se especifica en la cabecera Content-Type.

**¿Para que sirve?:**

    Actualización de información existente: Semánticamente, PUT se utiliza para actualizar datos existentes en el servidor, 
    similar a cómo una sentencia SQL UPDATE modifica datos en una base de datos.

    Comparación con sentencias SQL: Es comparable a UPDATE en SQL, ya que su objetivo principal es modificar datos existentes 
    en lugar de agregar nuevos.

## 3. ¿Para qué sirven los códigos de respuesta HTTP? 

    Los códigos de respuesta HTTP son parte integral del protocolo HTTP (Hypertext Transfer Protocol) y se utilizan para 
    comunicar el resultado de una solicitud realizada por un cliente (como un navegador web o una aplicación) al servidor.
    Estos códigos son importantes porque proporcionan información sobre el estado de la solicitud y la acción realizada 
    por el servidor. Aquí te explico para qué sirven y cómo se clasifican los códigos de respuesta:

    -Propósitos principales de los códigos de respuesta HTTP:

    1.- Indicar el resultado de la solicitud: Los códigos de respuesta HTTP indican si una solicitud fue exitosa, si hubo 
    algún problema o si se necesita alguna acción adicional por parte del cliente.

    2.- Facilitar la depuración y el diagnóstico: Ayudan a los desarrolladores y administradores de sistemas a diagnosticar
    problemas al identificar la naturaleza de la respuesta del servidor.

    3.- Gestionar flujos de trabajo: Permiten a las aplicaciones y sistemas gestionar flujos de trabajo según la respuesta 
    recibida, como redireccionar a una página de error, intentar nuevamente la solicitud, o mostrar mensajes de éxito.

## 4. Los códigos de respuesta HTTP se agrupan en 5 clases, investigue cuales son y que números abarcan. 

    Respuestas informativas (100–199):
        Indican que la solicitud ha sido recibida y el servidor está procesando la acción. Por ejemplo, 100 (Continue) 
        indica que el cliente puede continuar con la solicitud.

    Respuestas satisfactorias (200–299):
        Indican que la solicitud fue recibida, entendida y aceptada exitosamente. Por ejemplo, 200 (OK) indica que la
        solicitud fue exitosa.

    Redirecciones (300–399):
        Indican que el cliente necesita tomar más acciones para completar la solicitud. Por ejemplo, 301 (Moved Permanently)
        indica que la URL solicitada ha sido movida permanentemente a otra ubicación.

    Errores de los clientes (400–499):
        Indican que hubo un error en la solicitud del cliente. Por ejemplo, 404 (Not Found) indica que el recurso solicitado 
        no fue encontrado en el servidor.

    y errores de los servidores (500–599):
        Indican que hubo un error en el servidor al intentar completar la solicitud del cliente. Por ejemplo, 500 (Internal
        Server Error) indica un error genérico en el servidor que impide completar la solicitud.

## 5. Investigar los siguientes códigos de respuesta HTTP, mencione el nombre del código y explique que indica el mismo: 

**a. 200 (OK):**

    Indica que la solicitud ha tenido éxito. El servidor ha respondido adecuadamente y ha devuelto los recursos solicitados.

**b. 400 (Bad Request):**

    Indica que la solicitud del cliente no pudo ser entendida por el servidor debido a una sintaxis incorrecta o mal formada 
    en la solicitud. Es decir, hay un error en la solicitud del cliente.

**c. 401 (Unauthorized):**

    Indica que se requiere autenticación para acceder al recurso, pero el cliente no ha proporcionado las credenciales 
    adecuadas o estas son insuficientes. Es decir, el cliente debe autenticarse para obtener acceso.

**d. 403 (Forbidden):**

    Indica que el servidor entiende la solicitud del cliente, pero se niega a permitir que se complete. Esto puede deberse 
    a que el cliente no tiene permisos suficientes para acceder al recurso.

**e. 404 (Not Found):**

    Indica que el servidor no pudo encontrar el recurso solicitado. Es decir, la URL solicitada no existe en el servidor.

**f. 408 (Request Timeout):**

    Indica que el servidor ha esperado demasiado tiempo para recibir la solicitud del cliente. Esto puede ocurrir cuando 
    la conexión entre el cliente y el servidor es lenta o inestable.

**g. 500 (Internal Server Error):**

    Indica un error genérico en el servidor que impide completar la solicitud del cliente. Este código es utilizado cuando 
    el servidor encuentra una situación imprevista que le impide cumplir con la solicitud.

**h. 501 (Not Implemented):**

    Indica que el servidor no soporta la funcionalidad necesaria para completar la solicitud del cliente. Por ejemplo, 
    puede ser el resultado de una solicitud a un método HTTP no implementado por el servidor.

**i. 502 (Bad Gateway):**

    Indica que el servidor, actuando como una puerta de enlace o proxy, ha recibido una respuesta inválida desde un 
    servidor ascendente mientras intenta cumplir con la solicitud del cliente.

**j. 504 (Gateway Timeout):**

    Indica que el servidor, actuando como una puerta de enlace o proxy, no recibió una respuesta oportuna desde un 
    servidor ascendente al intentar cumplir con la solicitud del cliente.

## 6. ¿Qué es un endpoint?

    Un endpoint (o punto final) es un término utilizado en el contexto de las APIs (Interfaces de Programación de 
    Aplicaciones) y servicios web para referirse a un punto de acceso específico o una URL que es accesible desde 
    una red. En términos más simples, un endpoint es una URL que se utiliza para interactuar con un servicio web o 
    una API.

    -Características clave de un endpoint:

    1.- Dirección URL única: Cada endpoint tiene una dirección URL única que define dónde y cómo se puede acceder a un 
    recurso o funcionalidad específica del servicio o API.

    2.-Funcionalidad específica: Cada endpoint está asociado con una operación o recurso particular que el servicio web 
    o API proporciona. Por ejemplo, puede haber endpoints para obtener datos, enviar datos, actualizar información, 
    eliminar recursos, etc.

    3.-Métodos HTTP: Los endpoints suelen estar asociados con métodos HTTP específicos como GET, POST, PUT, DELETE, 
    entre otros, que determinan qué tipo de operación se realizará en el recurso del servidor.

    -Ejemplos de endpoints:

    1.- GET /api/users: Un endpoint que devuelve una lista de usuarios.
    2.- POST /api/users: Un endpoint que crea un nuevo usuario.
    3.- PUT /api/users/{id}: Un endpoint que actualiza la información de un usuario específico identificado por {id}.
    4.- DELETE /api/users/{id}: Un endpoint que elimina un usuario específico identificado por {id}.

    -Uso en servicios web y APIs:

    1.- Comunicación cliente-servidor: Los endpoints son utilizados por aplicaciones cliente (como aplicaciones web, 
    móviles, o scripts) para enviar solicitudes HTTP y recibir respuestas del servidor que ejecuta el servicio o API.

    2.- Estandarización y documentación: Es común que los servicios web y las APIs documenten sus endpoints junto con los 
    métodos HTTP soportados, los parámetros esperados, y los formatos de datos de respuesta, para que los desarrolladores 
    puedan integrar eficientemente los servicios en sus aplicaciones.

    -Importancia en el desarrollo de software:
    1.- Separación de responsabilidades: El uso de endpoints permite a los desarrolladores dividir las funcionalidades del 
    sistema en componentes discretos y accesibles a través de interfaces bien definidas, lo que facilita la integración 
    y el mantenimiento del software.