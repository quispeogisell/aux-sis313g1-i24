
# PRACTICA N°10

Docente: **Ing. Jose David Manami Figeroa** 

Auxiliar: **Univ. Sergio Moises Apaza Caballero**

Estudiante:  **Univ. Laura Gisel Quispe Olmedo** 

***Diseño y programacion grafica(SIS-313)***

## PREGUNTAS

### Para la práctica, subir un archivo .md como se indica en la parte de NOTA(S)(es decir, debe llevar el nombre “HW10_templateIdeas.md”).

**Presentar lo siguiente:**

- Link del template.

El primer enlace es el siguiente:

    https://www.figma.com/community/file/1000224878359129697

El segundo enlace es el siguiente:

    https://www.figma.com/community/file/990634575416780811

- Link del proyecto en Figma utilizando el template.

El primer enlace es el siguiente:

    https://www.figma.com/file/graZMgbt1bMLaDKZ9XAqMp/Option-2?type=design&node-id=0-1&mode=design&t=80xRv9nwQG5JCr7G-0

El segundo enlace es el siguiente:

    https://www.figma.com/file/9FDhkc42INbrU8RiAP3Irs/Option-1?type=design&node-id=1-265&mode=design&t=80xRv9nwQG5JCr7G-0

- Link del repositorio creado.
    
        https://gitlab.com/quispeogisell/react-with-lauragiselquispeolmedo.git



