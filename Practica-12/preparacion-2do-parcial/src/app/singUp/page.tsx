import Button from "@/components/Button";
import Logo from "@/components/Logo";
import Link from "next/link";
import { useRef } from "react";
import Image from "next/image";

export default function SingUp() {
    return(
        <section className="login container">
            <div className="login-img">
                <Image src="/images/singUp.svg" alt="" width={400} height={750}></Image>
            </div>
            <div>
                <Logo/>
                <div>
                    <h1>Sing Up</h1>
                    <p>Let’s get you all st up so you can access your personal account.</p>
                </div>
                <div>
                    <div className="signUp-ls">
                        <div>
                            <p>First Name</p>
                            <input type="text" id="username" name="username" />
                            <p>Gmail</p>
                            <input type="text" id="username" name="username" />
                            </div>
                        <div>
                            <p>Last Name</p>
                            <input type="text" id="username" name="username" />
                            <p>Phone Number</p>
                            <input type="text" id="username" name="username" />
                        </div>
                    </div>
                    <div>
                        <p>password</p>
                        <input type="password" id="pass" name="password" />
                        <p>Confirm Password</p>
                        <input type="password" id="pass" name="password" />
                        <p>Remember Me</p>
                        <p>Already have an account?</p>  
                        <Link href="/login">Login</Link> 
                    </div>
                    <div className="login-button">
                        <Button text="Create account"></Button>
                    </div>
                </div>       
            </div>
        </section>
    )
}