import Button from "@/components/Button";
import Logo from "@/components/Logo";
import Link from "next/link";
import Image from "next/image";

export default function Login() {
    return(
        <section className="login container">
            <div>
                <Logo/>
                <div>
                    <h1>Login</h1>
                    <p>Login to access your travelwise  account</p>
                </div>
                <div>
                    <div>
                        <p>Gmail</p>
                        <input type="text" id="username" name="username" />
                    </div>
                    <div>
                        <p>password</p>
                        <input type="password" id="pass" name="password" />
                        <p>Remember Me</p>
                        <p>Don’t have an account?</p>
                        <Link href="/singUp">Sing Up</Link>
                    </div>
                    <div className="login-button">
                        <Button text="Login"></Button>
                    </div>
                </div>       
            </div>
            <div className="login-img">
                <Image src="/images/login.svg" alt="" width={400} height={750}></Image>
            </div>
        </section>

    )
}