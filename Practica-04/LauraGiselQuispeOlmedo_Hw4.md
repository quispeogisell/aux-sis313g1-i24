#PRACTICA N°4

Docente: **Ing. Jose David Manami Figeroa**
Auxiliar: **Univ. Sergio Moises Apaza Caballero**
Estudiante:  **Univ. Laura Gisel Quispe Olmedo** 
***Diseño y programacion grafica(SIS-313)***

**Nivel Dificil-10**
***superado exitosamente***
![nivel-10](/Practica-04/Imagenes/level10.jpg)

---

**Nivel Dificil-18**
***superado exitosamente***
![nivel-18](/Practica-04/Imagenes/level18.jpg)

---

**Nivel Dificil-20**
***superado exitosamente***
![nivel-20](/Practica-04/Imagenes/level20.jpg)

---

**Nivel Dificil-29**
***superado exitosamente***
![nivel-29](/Practica-04/Imagenes/level29.jpgG)

---

**Nivel Dificil-30**
***superado exitosamente***
![nivel-30](/Practica-04/Imagenes/level30.jpg)

---

**Nivel Dificil-40**
***superado exitosamente***
![nivel-40](/Practica-04/Imagenes/level40.jpg)

---

**Nivel Dificil-45**
***superado exitosamente***
![nivel-45](/Practica-04/Imagenes/level45.jpg)

---

**Nivel Dificil-50**
***superado exitosamente***
![nivel-50](/Practica-04/Imagenes/level50.jpg)

---

**Nivel Dificil-60**
***superado exitosamente***
![nivel-60](/Practica-04/Imagenes/level60.jpg)

---

**Nivel Dificil-66**
***superado exitosamente***
![nivel-66](/Practica-04/Imagenes/level66.jpg)

---

**Nivel Dificil-70**
***superado exitosamente***
![nivel-70](/Practica-04/Imagenes/level70.jpg)

---

**Nivel Dificil-78**
***superado exitosamente***
![nivel-78](/Practica-04/Imagenes/level78.jpg)

---

**Nivel Dificil-80**
***superado exitosamente***
![nivel-80](/Practica-04/Imagenes/level80.jpg)